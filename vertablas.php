<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos.css">
    <title>Jugador</title>
</head>
<body>

    <a href="index.html"><img src="img/indice.png" alt="inicio" height="200"></a>
    
<center><h1>Tablas de la BBDD</h1></center>   

    
    <center><table class="formato">
        <thead>
            <tr><td style="background-color:rgb(119, 218, 140)">Dni</td>
                <td style="background-color:rgb(119, 218, 140)">Nombre</td>
                <td style="background-color: rgb(119, 218, 140)">Apellido</td>
                <td style="background-color: rgb(119, 218, 140)">Nacionalidad</td>
                <td style="background-color: rgb(119, 218, 140)">Equipo</td>
            </tr>                    
        </thead>
        <tbody>
            <?php
                require("php/usarbbdd2.php");

                echo("<h3>Jugadores actuales en la base de datos");

                $consulta = "SELECT * FROM jugadores ; ";

                $result=$mysqli->query($consulta);

                while ($rs = mysqli_fetch_array($result)) {
                    
                    echo ("<tr>"
                         ."<td>".$rs['dni']."</td>"
                         ."<td>".$rs['nombre']."</td>"
                         ."<td>".$rs['apellidos']."</td>"
                         ."<td>".$rs['nacionalidad']."</td>"
                         ."<td>".$rs['equipo']."</td>"
                         ."</tr>");
                }            
            ?>
        </tbody>
</table></center>


    <center><table class="formato">
        <thead>
            <tr><td style="background-color:rgb(192, 83, 68)">Id Equipo</td>
                <td style="background-color:rgb(192, 83, 68)">Nombre</td>
                <td style="background-color: rgb(192, 83, 68)">Ciudad</td>
                <td style="background-color: rgb(192, 83, 68)">Nº Socios</td>
            </tr>                    
        </thead>
        <tbody>
            <?php
                require("php/usarbbdd2.php");

                echo("<h3>Equipos actuales en la base de datos");

                $consulta = "SELECT * FROM equipos ; ";

                $result=$mysqli->query($consulta);

                while ($rs = mysqli_fetch_array($result)) {
                    
                    echo ("<tr>"
                         ."<td>".$rs['idequipo']."</td>"
                         ."<td>".$rs['nombre']."</td>"
                         ."<td>".$rs['ciudad']."</td>"
                         ."<td>".$rs['numsocios']."</td>"
                         ."</tr>");

                }            
            ?>
        </tbody>
</table></center>

    
</body>
</html>