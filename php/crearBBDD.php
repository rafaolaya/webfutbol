<?php
$servidor="localhost";
$usuario="root";
$clave=""; 

@$mysqli = new mysqli($servidor,$usuario,$clave);  //@ evita que se displayen los errores y avisos que genere la instrucción
if ($mysqli->connect_errno) {
    echo "Fallo al conectar a MySQL: " . $mysqli->connect_error. " ". $mysqli->connect_errno ;
    die (" SALIDA DEL PROGRAMA. Error acceso a BBDD");
}
else  echo ("SE HA CONECTADO AL SERVIDOR MySQL.");

// Si no existe creo la base de datos
$consulta ="CREATE DATABASE IF NOT EXISTS futbol";


if (!$resultado = $mysqli->query($consulta))
  {echo "Lo sentimos. La Aplicación no funciona<br>";
   echo "Error. en la consulta: ".$consulta."<br>";
   echo "Num.error: ".$mysqli->errno."<br>";
   echo "Error: ".$mysqli->error. "<br>";
   exit;
  }

    $mysqli->select_db("futbol");

// Se crean las tablas
    $consulta ="CREATE TABLE `equipos` (
      `idequipo` int(11) NOT NULL,
      `nombre` varchar(20) NOT NULL,
      `ciudad` varchar(20) NOT NULL,
      `numsocios` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";

if (!$resultado = $mysqli->query($consulta))
{echo "Lo sentimos. La Aplicación no funciona<br>";
 echo "Error. en la consulta: ".$consulta."<br>";
 echo "Num.error: ".$mysqli->errno."<br>";
 echo "Error: ".$mysqli->error. "<br>";
 exit;
}

  $consulta ="CREATE TABLE `jugadores` (
    `dni` varchar(9) NOT NULL,
    `nombre` varchar(20) NOT NULL,
    `apellidos` varchar(20) NOT NULL,
    `nacionalidad` varchar(30) NOT NULL,
    `equipo` int(11) DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";

if (!$resultado = $mysqli->query($consulta))
{echo "Lo sentimos. La Aplicación no funciona<br>";
echo "Error. en la consulta: ".$consulta."<br>";
echo "Num.error: ".$mysqli->errno."<br>";
echo "Error: ".$mysqli->error. "<br>";
exit;
}

// creo las claves de las tablas
$consulta ="ALTER TABLE `equipos`
ADD PRIMARY KEY (`idequipo`),
ADD UNIQUE KEY `idequipo` (`idequipo`);";

if (!$resultado = $mysqli->query($consulta))
{echo "Lo sentimos. La Aplicación no funciona<br>";
echo "Error. en la consulta: ".$consulta."<br>";
echo "Num.error: ".$mysqli->errno."<br>";
echo "Error: ".$mysqli->error. "<br>";
exit;
}

$consulta ="ALTER TABLE `jugadores`
ADD PRIMARY KEY (`dni`),
ADD KEY `equipo` (`equipo`) USING BTREE";

if (!$resultado = $mysqli->query($consulta))
{echo "Lo sentimos. La Aplicación no funciona<br>";
echo "Error. en la consulta: ".$consulta."<br>";
echo "Num.error: ".$mysqli->errno."<br>";
echo "Error: ".$mysqli->error. "<br>";
exit;
}

$consulta ="ALTER TABLE `equipos`
MODIFY `idequipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;";

if (!$resultado = $mysqli->query($consulta))
{echo "Lo sentimos. La Aplicación no funciona<br>";
echo "Error. en la consulta: ".$consulta."<br>";
echo "Num.error: ".$mysqli->errno."<br>";
echo "Error: ".$mysqli->error. "<br>";
exit;
}

$consulta ="ALTER TABLE `jugadores`
ADD CONSTRAINT `jugadores_ibfk_1` FOREIGN KEY (`equipo`) REFERENCES `equipos` (`idequipo`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;";

if (!$resultado = $mysqli->query($consulta))
{echo "Lo sentimos. La Aplicación no funciona<br>";
echo "Error. en la consulta: ".$consulta."<br>";
echo "Num.error: ".$mysqli->errno."<br>";
echo "Error: ".$mysqli->error. "<br>";
exit;
}












?>